"""AeroSpatial - Photogrammetry API Flights Class"""

# External Dependencies
import requests
import simplejson as json
import os

# Internal modules
import _authenticate

class API():
    def __init__(self):
        self._endpoint = 'https://cne5j8p3f2.execute-api.us-east-1.amazonaws.com/dev/'

class Account(API):

    def __init__(self):
        API.__init__(self)

    def listFlights(self):
        """Get flights specified by user"""
        # Construct URL
        url = self._endpoint + "flights/"
        # Authentication
        token = _authenticate.getIdToken()
        # Request
        headers = {
            'Authorization': token,
            }
        response = requests.request("GET", url, headers=headers).json()
        
        return (response)

    def deleteFlight(self, flightId):
        """Delete a flight"""
        # Construct URL
        url = self._endpoint + "flights/" + flightId
        # Authentication
        token = _authenticate.getIdToken()
        # Request
        headers = {
            'Authorization': token,
        }
        response = requests.request("DELETE", url, headers=headers).json()

        return (response)


class Flight(API):
    """Flight class for AeroSpatial"""

    def __init__(self, properties):
        """Initialise Class"""
        self.properties = properties
        print(self.properties)
        # stash a reference to id for ease
        self.id = self.properties['flightId']

    @classmethod
    def new(cls, name, metadata={}):
        """Create a new flight"""
        # Inherit API
        API.__init__(cls)
        # Construct URL
        url = cls._endpoint + "flights/"
        # Authentication
        token = _authenticate.getIdToken()
        # Request
        headers = {
            'Authorization': token,
        }
        properties = {
            'name': name,
            'metadata': metadata
        }
        response = requests.request("POST", url, json=properties, headers=headers).json()
        return cls(response)

    @classmethod
    def load(cls, flightId):
        """Get flights specified by flightId"""
        # Inherit API
        API.__init__(cls)
        # Construct URL
        url = cls._endpoint + "flights/" + flightId # can be null
        # Authentication
        token = _authenticate.getIdToken()
        # Request
        headers = {
            'Authorization': token,
            }
        response = requests.request("GET", url, headers=headers).json()
        return cls(response)
    
    def upload(self, filename):
        """Get signed URL for image upload"""
        # Construct URL
        basename = os.path.basename(filename)
        url = self._endpoint + "flights/" + self.id + "/images/?filename=" + basename
        # Authentication
        token = _authenticate.getIdToken()
        # Request
        headers = {
            'Authorization': token,
        }
        response = requests.request("GET", url, headers=headers).json()
        # File
        _file = {'file': open(filename, 'rb')}
        # Upload
        response = requests.request("POST", response['url'], data=response['fields'], files=_file)
        return {'file': basename, 'response': response}

    def build(self, settings):
        """Start a build"""
        # Construct URL
        url = self._endpoint + "flights/" + self.id 
        # Authentication
        token = _authenticate.getIdToken()
        # Request
        headers = {
            'Authorization': token,
        }
        response = requests.request("POST", url, json=settings, headers=headers).json()
        return response

    # TODO download()

