import boto3, configparser, os
from pathlib import Path

# add aws credential placeholders because boto3 is broken
# https://stackoverflow.com/questions/47849702/aws-boto-warrant-library-srp-authentication-and-credentials-error
os.environ['AWS_ACCESS_KEY_ID'] = ''
os.environ['AWS_SECRET_ACCESS_KEY'] = ''

client = boto3.client('cognito-idp', region_name = 'us-east-1')

def _getJWT(username, password):
    return (client.initiate_auth(
        AuthFlow= 'USER_PASSWORD_AUTH',
        AuthParameters={
            'USERNAME': username,
            'PASSWORD': password
        },
        # AWS AeroSpatial App
        ClientId='375tgtnjibbe85rdbkt2h6grka'
    ))

def _readCredentials():
    config = configparser.ConfigParser()
    credentials = (str(Path.home()) + '/.aerospatial/credentials')
    config.read(credentials)
    return {
        'username': config['default']['username'],
        'password': config['default']['password']
        }

def getIdToken():
    credentials = _readCredentials() 
    jwt = _getJWT(credentials['username'], credentials['password'])
    return jwt['AuthenticationResult']['IdToken']
