# python-sdk

AeroSpatial API Python SDK

v0.0.1-alpha

This work is still in development.

### Quickstart

```python
from aerospatial import Flight
import os, glob # python modules

# 1 Create flight
flight = Flight.new('Field Survey 01', {'aircraft': 'drone', 'date':'2018-06-01'})

print('FlightId: ', flight.id) # -> 'FlightId: e462e9dc-326f-4287-bf05-3e2cc1765d63'

# 2 Upload images
images = glob.glob('*.jpg')
for image in images:
    flight.upload(image)

# 3 Start build
flight.build({'orthophoto-resolution': 20.0})

```

Please read the documentation for authentication setup.

### Documentation
[docs.aerospatial.io](https://docs.aerospatial.io)

### Install

Installation via a Python package is on the todo list.
